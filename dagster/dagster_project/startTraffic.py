import traceback
import os
import re
import time
import json
from typing import List, NoReturn

from dagster import op, graph, job, fs_io_manager, Out, Output, OpExecutionContext
from dagster import Failure, failure_hook, HookContext

from .params import input_params
from .resources import resource_defs
from .utils import *
from . import ue
from . import workflowCreds

#
# start() drags stuff out of the incoming params to pass along.
#
@op(config_schema={"params" : dict},
    required_resource_keys={"sshClient", "MyLogger"},
    out={"params": Out()})
def startPingiPerf(context):
    params = context.op_config["params"]
    logger = context.resources.MyLogger
    cpe = params["CPE"]

    ue.attachUE(context, params)
    
    logger.log("Starting traffic generators")
    command = "mavenir-workflow/start-traffic.ps1"
    results = runRemoteCommand(context, params,
                               params["windows_username"], cpe, command)
    logger.log("Traffic generators are started")
    pass

@job(
    description = "Start long running Ping and iPerf",
    config = {
        "ops" : {
            "startPingiPerf" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        }
    },
    resource_defs = resource_defs
)
def startTraffic():
    startPingiPerf()
    pass

