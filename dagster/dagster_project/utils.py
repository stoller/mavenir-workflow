from typing import List, NoReturn
import traceback

#
# Goofy
#
__all__ = [
    "raiseFailure", "runRemoteCommand",
    "updateExecution", "addAttachment",
    "logInfo", "clearLog", "saveLog",
]

# We want to pass the blob into the hook from every failure
def raiseFailure(status, description) -> NoReturn:
    raise Failure(metadata={"status" : status}, description=description)

# Handy Helper
def runRemoteCommand(context, params, user, node, command, timeout=45):
    sshClient = context.resources.sshClient
    
    try:
        index = sshClient.runRemoteCommand(user, node, command, timeout=timeout);
    except:
        context.log.info(traceback.format_exc())
        raiseFailure(params, "Command failed (connection): " + command)
        pass

    if sshClient.getRemoteExitStatus(index):
        raiseFailure(params, "Command failed (status): " + command)
        pass

    results = sshClient.getRemoteResults(index)
    return results

#
# Wrapper.
#
def updateExecution(context, params, execution, result, comments=None):
    if params["skip_tcms"]:
        return
    
    kiwiTCMS = context.resources.kiwiTCMS
    kiwiTCMS.updateExecution(params, execution, result, comments=comments)
    pass

def addAttachment(context, params, filename, content):
    if params["skip_tcms"]:
        return
    
    kiwiTCMS = context.resources.kiwiTCMS
    kiwiTCMS.addAttachment(params, filename, content)
    pass

#
# I want to save all the log info in Kiwi.
#
def logInfo(context, params, message):
    context.resources.MyLogger.log(message)
    return

def clearLog(context, params):
    context.resources.MyLogger.clear()
    return

def saveLog(context, params):
    addAttachment(context, params, "dagster-log",
                  context.resources.MyLogger.get())
    return

