import time

from dagster import DagsterError, DagsterExecutionInterruptedError
from .utils import *

#
# Windows.
#
QUECTEL_CONTROL  = "python3.12 /Users/stoller/mavenir-workflow/quectel_control.py"

#
# As per discussion with Dustin, we want to make sure that we have a consistent
# initial state, with first attach to ru1 every time. So drive the attenuation
# to ru1 before we attach,
#
def attachUE(context, params, execution=None):
    attach = QUECTEL_CONTROL + " up"

    logInfo(context, params, "Attaching UE")
    runRemoteCommand(context, params,
                     params["windows_username"], params["CPE"], attach)
    logInfo(context, params, "Waiting a moment")
    time.sleep(3)
    logInfo(context, params, "Running a ping to confirm attach")

    #
    # We want to catch this failure and abort
    #
    pingResults = ""
    try:
        pingResults = runPing(context, params, count=5)
        status = "PASSED"
    except DagsterExecutionInterruptedError:
        raise
    except:
        status = "FAILED"
        pass

    if execution:
        updateExecution(context, params, execution, status,
                        comments=[pingResults])
        pass

    return status == "PASSED"

#
# Detach and run a ping test to confirm
#
def detachUE(context, params, execution=None):
    command = QUECTEL_CONTROL + " down"

    logInfo(context, params, "Detaching UE")
    runRemoteCommand(context, params,
                     params["windows_username"], params["CPE"], command)
    logInfo(context, params, "Waiting a moment")
    time.sleep(2)
    logInfo(context, params, "Running a ping to confirm detach")
    #
    # We want this to fail, but catch it cause that is good.
    #
    pingResults = ""
    try:
        pingResults = runPing(context, params, count=5)
        status = "FAILED"
    except DagsterExecutionInterruptedError:
        raise
    except:
        status = "PASSED"
        pass

    if execution:
        updateExecution(context, params, execution, status,
                        comments=[pingResults])
        pass
    return

#
# Run a ping test
#
def runPing(context, params, count=10):
    command = "ping -n " + str(count) + " " + params["server"]
    logInfo(context, params, command)

    results = runRemoteCommand(context, params,
                               params["windows_username"], params["CPE"], command)
    return results

#
# Run an iperf test
#
def runIperf(context, params, timeout=5, direction="bidir", proto="UDP"):
    command  = "iperf3 -V -p 5000 --format m -t " + str(timeout) + " "
    if proto == "UDP":
        command += "-u -b 80M -l 1300 "
        pass
    if direction == "bidir":
        command += "--bidir "
    elif direction == "download":
        command += "-R "
        pass
    command += "-c " + params["server"]
    logInfo(context, params, command)

    results = runRemoteCommand(context, params,
                               params["windows_username"], params["CPE"], command,
                               timeout=timeout+15)
    return results

