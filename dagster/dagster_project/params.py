import tempfile

# Place holders
EMULAB_UID        = "leebee"
EMULAB_PID        = "Mavenir"
INFLUXDB_HOSTPORT = "foo:8086"
INFLUXDB_PASSWORD = ""

# The workflow will create this import file so we have the actual user/project
try:
    from . import workflowDefs
    EMULAB_UID = workflowDefs.EMULAB_UID
    EMULAB_PID = workflowDefs.EMULAB_PID
    INFLUXDB_HOSTPORT = workflowDefs.INFLUXDB_HOSTPORT
    INFLUXDB_PASSWORD = workflowDefs.INFLUXDB_PASSWORD
except ImportError:
    pass    

input_params = {
    'CPE' : "sm06.emulab.net",
    'server' : "2001:1000:18:100::10",
    'windows_username' : "stoller",
    'proj' : EMULAB_PID,
    'username' : EMULAB_UID,
    'sut_profile_name' : "Maviner,mavenir-mmw-devices",
    'sut_experiment_name' : "mmw-devices",
    'sut_paramset' : None,
    'skip_tcms' : True,
    'debug' : False,
    'condensed': False,
    'influxHost' : INFLUXDB_HOSTPORT,
    'influxPassword' : INFLUXDB_PASSWORD
}
