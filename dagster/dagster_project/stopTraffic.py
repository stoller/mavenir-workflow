import traceback
import os
import re
import time
import json
from typing import List, NoReturn

from dagster import op, graph, job, fs_io_manager, Out, Output, OpExecutionContext
from dagster import Failure, failure_hook, HookContext

from .params import input_params
from .resources import resource_defs
from .utils import *
from . import ue
from . import workflowCreds

@op(config_schema={"params" : dict},
    required_resource_keys={"sshClient", "MyLogger"})
def stopPingiPerf(context: OpExecutionContext):
    params = context.op_config["params"]
    logger = context.resources.MyLogger
    cpe    = params["CPE"]

    logger.log("Stopping traffic generators")
    
    command = "mavenir-workflow/stop-traffic.ps1"
    results = runRemoteCommand(context, params,
                               params["windows_username"], cpe, command)
    logger.log("Traffic generators stopped")
    ue.detachUE(context, params)
    pass

@job(
    description = "Stop long running Ping and iPerf",
    config = {
        "ops" : {
            "stopPingiPerf" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        }
    },
    resource_defs = resource_defs
)
def stopTraffic():
    stopPingiPerf()
    pass

