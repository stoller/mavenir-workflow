from dagster import Definitions, in_process_executor
from . import resources

from . import runTests
from . import startTraffic
from . import stopTraffic

defs = Definitions(
    jobs=[
        runTests.runTests,
        startTraffic.startTraffic,
        stopTraffic.stopTraffic
    ],
    resources=resources.resource_defs,
    executor=in_process_executor,
)
