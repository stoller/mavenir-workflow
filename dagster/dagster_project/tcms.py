#
# TCMS/KIWI
#
import sys
import os
import dagster_kiwitcms as kiwi

#
# KIWI setup for the Mavenir gear.
#
kiwi_params = {
    "classification": "Mavenir",
    "product": "Mavenir Gear",
    "version": "1.0",
    "product_description": "Mavenir Gear",
    "plantype": "Integration",
    "testplan": "Mavenir Test",
    "attach_summary": "Attach Test",
    "handover_summary": "Handover Test",
    "ping_summary": "Latency Test",
    "iperf_summary": "Throughput Test",
    "detach_summary": "Detach Test",
    # These are defaults from the Stackstorm Kiwi actions
    "testcase_status": "CONFIRMED",
    "testcase_priority": "P1",
    "testcase_category": "--default--",
    "testrun_manager": "admin",
}

class kiwiTCMS:
    def __init__(self, creds):
        self.creds = creds
        self.kiwi  = kiwi.KiwiTCMSBaseAction(creds)
        pass

    def setup(self, status):
        kiwitcms = self.kiwi
        kiwidata = status["kiwidata"]
        params   = status

        #
        # Create a few things that are always needed and never change.
        #
        result = kiwitcms.run(action="classification.get",
                              name=kiwi_params["classification"])
        kiwidata["classification"] = result["id"]

        result = kiwitcms.run(action="product.ensure",
                              name=kiwi_params["classification"],
                              description=kiwi_params["product_description"],
                              classification=kiwidata["classification"])
        kiwidata["product"] = result["id"]

        result = kiwitcms.run(action="version.ensure",
                              product=kiwidata["product"],
                              value=kiwi_params["version"])
        kiwidata["version"] = result["id"]
    
        result = kiwitcms.run(action="plantype.ensure",
                              name=kiwi_params["plantype"])
        kiwidata["plantype"] = result["id"]

        result = kiwitcms.run(action="build.ensure",
                              name=kiwi_params["version"],
                              version=kiwidata["version"])
        kiwidata["build"] = result["id"]

        #
        # Create a stub testplan to hold testcases since they have to be
        # associated with a plan.
        #
        plan_name = kiwi_params["testplan"]
        result = kiwitcms.run(action="testplan.ensure",
                              name=plan_name,
                              text=plan_name,
                              type=kiwidata["plantype"],
                              product=kiwidata["product"],
                              product_version=kiwidata["version"])
        kiwidata["testplan"] = result["id"]

        #
        # Test cases as they get created. Indexed by testcase "summary"
        #
        kiwidata["testcases"] = {}
        pass

    #
    # Create a new TestRun. We throw away the old test run since we do one
    # at a time and never need an old one again.
    #
    def createTestRun(self, status, summary, tags=None):
        kiwitcms  = self.kiwi
        kiwidata  = status["kiwidata"]

        if status["skip_tcms"]:
            return
        
        #
        # We will add TestCases as needed.
        #
        summary = "Mavenir " + summary + " Test"
        result = kiwitcms.run(action="testrun.create",
                              summary=summary,
                              manager=kiwi_params["testrun_manager"],
                              build=kiwidata["build"],
                              plan=kiwidata["testplan"],
                              tags=tags)
        kiwidata["testrun"] = result["id"]

        #
        # A test execution is a TestCase that has been added to a TestRun.
        # We need to remember it so we can find the execution ID.
        #
        kiwidata["test_executions"] = {}
        pass
    
    #
    # Create a testcase if it does not already exist. It is added to our
    # stub plan since it has to be in a plan.
    #
    def createTestCase(self, status, summary):
        kiwitcms  = self.kiwi
        kiwidata  = status["kiwidata"]
        testcases = kiwidata["testcases"]

        if summary in testcases:
            testcase_id = testcases[summary]
        else:
            result = kiwitcms.run(action="testcase.ensure",
                                  summary=summary,
                                  text=summary,
                                  testplan=kiwidata["testplan"],
                                  status=kiwi_params["testcase_status"],
                                  category=kiwi_params["testcase_category"],
                                  priority=kiwi_params["testcase_priority"],
                                  product=kiwidata["product"])

            testcase_id = result["id"]
            testcases[summary] = testcase_id;
            pass
        return testcase_id

    #
    # Create and add a testcase to the current run.
    #
    def addTestCase(self, status, summary):
        kiwitcms    = self.kiwi
        kiwidata    = status["kiwidata"]

        if status["skip_tcms"]:
            return
        
        testcase_id = self.createTestCase(status, summary)
        executions  = kiwidata["test_executions"]

        result = kiwitcms.run(action="testrun.addcase",
                              run=kiwidata["testrun"],
                              case=testcase_id,
                              status="BLOCKED")

        executions[summary] = {
            "testcase_id"  : testcase_id,
            "testrun_id"   : result["id"],
            "summary"      : summary,
        }
        return executions[summary]

    #
    # Update test execution status.
    #
    def updateExecution(self, status, execution, result, comments=None):
        if not status["skip_tcms"]:
            self.kiwi.run(action="testexecution.update",
                          execution=execution["testrun_id"],
                          status=result, comments=comments)
            pass
        pass

    #
    # Another helper to attach files to a run.
    #
    def addAttachment(self, status, filename, content):
        if not status["skip_tcms"]:
            self.kiwi.run(action="testrun.addattachment",
                          run=status["kiwidata"]["testrun"],
                          filename=filename,
                          content=content)
            pass
        pass

    pass

