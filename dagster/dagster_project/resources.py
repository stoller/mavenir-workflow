from datetime import datetime

from dagster import resource, ConfigurableResource, InitResourceContext
from dagster import fs_io_manager, get_dagster_logger
from pydantic import PrivateAttr

import dagster_emulab as portal
import dagster_paramiko as support
from . import workflowCreds
from . import tcms

@resource(config_schema={"creds": dict})
def emulabPortal(context):
    creds = context.resource_config["creds"]
    return portal.EmulabBaseAction(creds)

@resource(config_schema={"creds": dict})
def sshClient(context):
    creds = context.resource_config["creds"]
    return support.sshClient(creds)

@resource(config_schema={"creds": dict})
def kiwiTCMS(context):
    creds = context.resource_config["creds"]
    return tcms.kiwiTCMS(creds)

#
# For this to work, we need to specify the "in_process_executor" in the
# Definitions (see __init__.py). Which is fine for what we generally use
# Dagster for. 
#
class MyLogger(ConfigurableResource):
    _log: str = PrivateAttr()

    def setup_for_execution(self, context: InitResourceContext) -> None:
        self._log = ""
        pass
    
    def log(self, string):
        get_dagster_logger().info(string)
        now = datetime.now()
        t = now.strftime('%I:%M:%S %p: ')
        self._log += t + string + "\n"
        pass

    def clear(self):
        self._log = ""

    def get(self):
        return self._log

    def dump(self):
        get_dagster_logger().info(self._log)

    pass

#
# resource definitions for jobs.
#
resource_defs = {
    "io_manager": fs_io_manager,
    "portal"    : emulabPortal.configured({"creds" : workflowCreds.portalCreds}),
    "sshClient" : sshClient.configured({"creds" : workflowCreds.managerCreds}),
    "kiwiTCMS"  : kiwiTCMS.configured({"creds" : workflowCreds.kiwiCreds}),
    "MyLogger"  : MyLogger.configure_at_launch(),
}
