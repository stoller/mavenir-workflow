import traceback
import os
import re
import time
import json
import statistics
from datetime import datetime
from typing import List, NoReturn
from tabulate import tabulate

from dagster import op, graph, job, fs_io_manager, Out, Output
from dagster import DagsterError, DagsterExecutionInterruptedError
from dagster import execute_job, reconstructable
from dagster import Failure, failure_hook, HookContext, get_dagster_logger
from dagster_shell import execute_shell_command

from .params import input_params
from .resources import resource_defs
from .utils import *
from . import ue
from . import workflowCreds

#
# Decorator for the @op decorator which sends the same arguments to every op.
# Cause for some reason, there is no way to do this in the @job decorator.
# And I hate typing the same thing over and over. 
#
# This needs to go someplace else, but only using it here for now. Just
# playing around with it. 
#
def myop(opfunc):
    func = op(required_resource_keys={"sshClient", "kiwiTCMS", "MyLogger"})
    return func(opfunc)

#
# start() drags stuff out of the incoming params to pass along.
#
@op(config_schema={"params" : dict},
    required_resource_keys={"kiwiTCMS"},
    out={"params": Out()})
def start(context) -> dict:
    params  = context.op_config["params"]
    params  = params.copy()
    
    #
    # This gets carried along
    #
    params["ue"]       = params["CPE"];
    params["kiwidata"] = {};
    params["log"]      = "";

    if params["skip_tcms"]:
        context.log.info("TCMS/KIWI is disabled")
    else:
        context.log.info("TCMS/KIWI is enabled")
        kiwiTCMS = context.resources.kiwiTCMS
        kiwiTCMS.setup(params)
        # Debugging
        if False:
            kiwiTCMS.createTestRun(params, "Tester")
            attach = kiwiTCMS.addTestCase(params, "Attach")
            kiwiTCMS.updateExecution(params, attach, "PASSED", comments=["Bla"])
            detach = kiwiTCMS.addTestCase(params, "Detach")
            kiwiTCMS.updateExecution(params, Detach, "PASSED", comments=["Alb"])
            pass
        pass
    context.log.debug(str(params))
    return params

#
# Stub to join the conditional
#
@myop
def initTests(context, params: dict):
    if False:
        raiseFailure(params, "Aborting")
        pass
    
    #
    # Make sure the ping/iperf traffic generators are stopped
    #
    logInfo(context, params, "Making sure traffic generators are stopped")
    command = "mavenir-workflow/stop-traffic.ps1"
    results = runRemoteCommand(context, params,
                               params["windows_username"], params["ue"], command)

    return params

#
# Run Ping and iperf
#
@myop
def runAllTests(context, params: dict):
    kiwiTCMS  = context.resources.kiwiTCMS
    sshClient = context.resources.sshClient
    iperftime = 30
    pingcount = 30
    if params["debug"]:
        iperftime  = 10
        pingcount  = 10
        pass

    if False:
        return params

    clearLog(context, params)

    ue.attachUE(context, params)

    # Create TestRun and associated TestCases.
    kiwiTCMS.createTestRun(params, "Ping/Iperf Tests")

    pingTE  = kiwiTCMS.addTestCase(params, "Latency")
    iperfTE = kiwiTCMS.addTestCase(params, "Throughput")

    #
    # We want to catch this failure and abort
    #
    logInfo(context, params, "Running a ping, patience please")
    pingResults = ""
    try:
        pingResults = runPing(context, params, count=pingcount)
        status = "PASSED"
    except DagsterExecutionInterruptedError:
        raise
    except Exception as exc:
        context.log.debug(str(exc))
        status = "FAILED"
        pass

    addAttachment(context, params, "latency", pingResults)
    ping_summary = ""
    if status != "FAILED":
        for line in pingResults.splitlines():
            if "Ping statistics" in line:
                ping_summary = ""
                pass
            if ping_summary != None:
                ping_summary += line + "\n"
                pass
            pass
        pass
    if params["condensed"]:
        logInfo(context, params, "ping complete")
    else:
        logInfo(context, params, ping_summary)
        pass
    updateExecution(context, params, pingTE, status, comments=[ping_summary])
    
    if status == "FAILED":
        raiseFailure(params, "Ping failed, aborting")
        pass

    logInfo(context, params, "Running iperf, this will take a little while")
    iperfResults = runIperf(context, params, timeout=iperftime,
                            direction="bidir", proto="TCP")

    #
    # Break out the last lines (summary) for a comment
    #
    iperf_summary = None
    for line in iperfResults.splitlines():
        if "Summary Results" in line:
            iperf_summary = ""
            pass
        if iperf_summary != None:
            iperf_summary += line + "\n"
            pass
        pass
    if params["condensed"]:
        logInfo(context, params, "iperf complete")
    else:
        logInfo(context, params,iperf_summary)
        pass
    updateExecution(context, params, iperfTE, "PASSED", comments=[iperf_summary])
    addAttachment(context, params, "throughput", iperfResults)

    ue.detachUE(context, params)
    saveLog(context, params)
    return params
    
#
# Cleanup
#
@myop
def cleanup(context, params: dict):
    #
    # Restart the ping/iperf traffic generators
    #
    #logInfo(context, params, "Restarting traffic generators")
    #command = "mavenir-workflow/start-traffic.ps1"
    #results = runRemoteCommand(context, params, params["ue"], command)

    return params

@job(
    description = "Run short Ping and iPerf tests",
    config = {
        "ops" : {
            "start" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        }
    },
    resource_defs = resource_defs
)
def runTests():
    params = start()
    params = initTests(params)
    params = runAllTests(params)
    params = cleanup(params)
    pass

