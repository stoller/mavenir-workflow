from setuptools import find_packages, setup

setup(
    name="dagster_project",
    version="0.1",
    packages=['dagster_project'],
    install_requires=[
        'dagster-emulab @ git+https://gitlab.flux.utah.edu/powder-workflows/dagster-emulab',
        'dagster-paramiko @ git+https://gitlab.flux.utah.edu/powder-workflows/dagster-paramiko',
        'dagster-kiwitcms @ git+https://gitlab.flux.utah.edu/powder-workflows/dagster-kiwitcms',
        'pytest'
    ],
)
