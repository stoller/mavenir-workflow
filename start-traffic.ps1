chdir C:\Users\stoller\mavenir-workflow

nssm start TOTA-Ping
if ($LASTEXITCODE) {
   write-host "Could not start the ping process";
   exit 1
}
nssm start TOTA-iPerf
if ($LASTEXITCODE) {
   write-host "Could not start the iperf process";
   exit 1
}
