#!/bin/sh
set -x

SCRIPTNAME=$0
TMPDIR="/var/tmp"
cd $TMPDIR

sudo apt-get update
sudo apt-get -y install --no-install-recommends daemon libfile-tail-perl
if [ $? -ne 0 ]; then
    echo 'ERROR: install pip/daemon failed'
    exit 1
fi

#
# Latest version of iperf fixes the server side hang problem.
#
wget https://www.emulab.net/downloads/iperf-3.16.tar.gz
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not download iperf3 tarfile'
    exit 1
fi
tar -C $TMPDIR -zxf iperf-3.16.tar.gz
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not unpack iperf3 tarfile'
    exit 1
fi
(cd $TMPDIR/iperf-3.16; ./configure; make)
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not configure/build iperf3'
    exit 1
fi
(cd $TMPDIR/iperf-3.16; sudo make install)
if [ $? -ne 0 ]; then
    echo 'ERROR: Could not install iperf3'
    exit 1
fi
# The makefile in runs ldconfig incorrectly.
sudo ldconfig

sudo touch /etc/.iperf-installed
