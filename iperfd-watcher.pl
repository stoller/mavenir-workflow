#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use File::Tail;
use POSIX;

sub usage()
{
    print "Usage: iperf-watcher\n";
    exit(1);
}
my $optlist   = "n";
my $impotent  = 0;
my $TMPDIR    = "/var/tmp";
my $IPERFIP   = "192.168.2.128";
$IPERFIP = "155.98.36.139";

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"n"})) {
    $impotent++;
}
usage()
    if (@ARGV);


sub Watcher($)
{
    my ($bus) = @_;
    my $port    = 50000 + $bus;
    my $pidfile = "/var/tmp/iperf-${bus}.pid";
    my $logfile = "/var/tmp/iperf-${bus}.log";
    my $counter = 0;

    if (-e $pidfile) {
	my $pid     = `/bin/cat $pidfile`;
	chomp($pid);
	print "Bus $bus: Killing running $pid\n";
	if (!$impotent) {
	    system("sudo kill $pid");
	    system("sudo /bin/rm -f $pidfile");
	}
    }

    my $pid = fork();
    if ($pid) {
	return $pid;
    }

    while (1) {
	print "Bus $bus: Starting iperf server on port $port\n";
	if (!$impotent) {
	    my $command = "/bin/iperf3 -s -p $port -D -B $IPERFIP ".
		" --pidfile $pidfile --logfile $logfile";
	    print "Bus $bus: $command\n";
	    
	    system("$command");
	    if ($?) {
		die("Could not start iperf server for bus $bus on port $port");
	    }
	}
	while (! -e $pidfile) {
	    print "Bus $bus: Waiting for pid file to appear\n";
	    sleep(1);
	}
	my $pid = `/bin/cat $pidfile`;
	chomp($pid);
	
	while (! -e $logfile) {
	    print "Bus $bus: Waiting for log file to appear\n";
	    sleep(1);
	}
	my $file = File::Tail->new("name" => $logfile,
				   "maxinterval" => 1,
				   "interval"    => 1,
				   "adjustafter" => 5,
				   "reset_tail"  => 0);

	while (my $line = $file->read()) {
	    if ($line =~ /\s([\d\.]+) (\w+)\s+([\d\.]+) (\w+)\/sec/) {
		print "Bus $bus: $1, $3\n";
		my $bw     = $1;
		my $rate   = $3;

		if ($bw == 0 && $rate == 0) {
		    $counter++;

		    if ($counter > 10) {
			print "Bus $bus: Killing iperf server $pid\n";
			if (!$impotent) {
			    system("sudo kill $pid");
			    system("sudo /bin/rm -f $pidfile");
			}
			last;
		    }
		}
	    }
	}
	sleep(2);
	$counter = 0;
    }
    exit(0);
}
#
# iperf servers. One for every possible bus. Silly, I know. 
#
foreach my $bus (6183) {
    print "Starting watcher for $bus\n";
    Watcher($bus);
}
while (1) {
    sleep(5);
}
exit(0);

