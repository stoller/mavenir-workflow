chdir C:\Users\stoller\mavenir-workflow

nssm stop TOTA-Ping
if ($LASTEXITCODE) {
   write-host "Could not stop the ping process";
   exit 1
}
nssm stop TOTA-iPerf
if ($LASTEXITCODE) {
   write-host "Could not stop the iperf process";
   exit 1
}
exit 0
