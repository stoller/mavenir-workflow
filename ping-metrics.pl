#!/usr/bin/perl -w
#
# Copyright (c) 2000-2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

use lib ".";
use workflow;

sub usage()
{
    print "Usage: ping-metrics\n";
    exit(1);
}
my $optlist   = "dnbK";
my $debug     = 0;
my $impotent  = 0;
my $daemon    = 0;
my $childpid;
my $PIDFILE   = "/Windows/Temp/ping-metrics.pid";
my $SERVER    = "2001:1000:18:100::10";
#$SERVER    = "155.98.36.1";

#
# Turn off line buffering on output
#
$| = 1;

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"d"})) {
    $debug++;
}
if (defined($options{"n"})) {
    $impotent++;
}
if (defined($options{"b"})) {
    $daemon++;
}
usage()
    if (@ARGV);

#
# Kill 
#
if (defined($options{"K"})) {
    exit(0)
	if (! -e $PIDFILE);

    if (open(IN, $PIDFILE)) {
	my $pid = <IN>;
	chomp($pid);
	close(IN);
	system("taskkill.exe /PID $pid /F /T");
	if ($?) {
	    if ($? >> 8 != 128) {
		die("Could not kill $pid");
	    }
	}
	unlink($PIDFILE);
    }
    else {
	die("Could not open $PIDFILE: $!");
    }
    exit(0);
}

#
# Setup a signal handler for newsyslog.
#
sub handler()
{
    print "Handler: $childpid\n";
    system("taskkill /PID $childpid /F /T");
    unlink($PIDFILE) if (-e $PIDFILE);
    exit(0);
}
$SIG{INT} = \&handler;
$SIG{KILL} = \&handler;
$SIG{QUIT} = \&handler;

sub RunPing()
{
    my $command = "ping -t $SERVER";

    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    $childpid = open(PIPE, "$command |");
    if (!defined($childpid)) {
	die("popen");
    }
    if ($childpid) {
	while (<PIPE>) {
	    print $_ if ($debug);
	    if ($_ =~ / time=([\d\.]+)ms/ ||
		$_ =~ / time\<(\d)ms/) {
		UploadPingMetric($1);
	    }
	}
	close(PIPE);
    }
}

sub UploadPingMetric($)
{
    my ($latency) = @_;
    #print "Latency: $latency\n";

    my $url = "https://${INFLUXDB_HOSTNAME}:${INFLUXDB_PORT}/write";
    $url .= "?db=${INFLUXDB_DATABASE}";

    my $command =
	"curl.exe --insecure -m 10 -s -q -XPOST ".
	"  --data-binary \"ping,${INFLUXDB_GLOBAL_TAGS} latency=$latency\" ".
	"  -u ${INFLUXDB_USERNAME}:${INFLUXDB_PASSWORD} \"$url\"";

    print "$command\n" if ($debug);
    if (!$impotent) {
	system($command);
    }
}

#
# Loop, just keep restarting the ping. 
#
print "PID:$PID\n";

unlink($PIDFILE);
if (open(P, ">$PIDFILE")) {
    print P "$PID\n";
    close(P);
}
else {
    die("Could not create $PIDFILE: $!");
}

while (1) {
    print "Starting up ping ...\n" if ($debug);
    RunPing();
    print "Ping has exited, waiting a moment ...\n" if ($debug);
    sleep(5);
}
