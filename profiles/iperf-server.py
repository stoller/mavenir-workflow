"""Add a node to the mavenir share lan and start an iperf server
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Emulab extension
import geni.rspec.emulab

SHVLANNAME = "mav-n6-shvlan"
IPERFSTART = "/local/repository/start-iperfd.pl"

# Need this route.
# ip route add 192.168.0.0/16 via 192.168.50.1
#

# Create a portal context.
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Add a raw PC to the request and give it an interface on the shared vlan
node1 = request.RawPC("iperf-server")
iface1 = node1.addInterface()
iface1.addAddress(pg.IPv4Address("192.168.50.10", "255.255.255.0"))
node1.addService(pg.Execute(shell="bash", command=IPERFSTART))

# Connect up to an existing shared vlan
link = request.Link("shared-vlan")
link.addInterface(iface1)
link.best_effort = True
link.link_multiplexing = True

link.connectSharedVlan(SHVLANNAME);

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
