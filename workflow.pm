#!/usr/bin/perl -wT
#
# Copyright (c) 2000-2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
package workflow;
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(
    $INFLUXDB_HOSTNAME
    $INFLUXDB_PORT
    $INFLUXDB_USERNAME
    $INFLUXDB_PASSWORD
    $INFLUXDB_BUCKET
    $INFLUXDB_ORG
    $INFLUXDB_DATABASE
    $INFLUXDB_GLOBAL_TAGS
    $INFLUXDB_SSL
    );

# Influx tag
my $HOST = `hostname`;
chomp($HOST);
if ($HOST =~ /^([^\.]+)\.(.*)$/) {
    $HOST = $1;
}

#
# This stuff needs to be auto generated.
#
#$INFLUXDB_HOSTNAME="node-0.mavenir-workflow.mavenir.emulab.net";
$INFLUXDB_HOSTNAME="155.98.36.101";
$INFLUXDB_PORT="8447";
$INFLUXDB_PASSWORD="bc39c1500dfa";
$INFLUXDB_USERNAME="metrics";
$INFLUXDB_BUCKET="metrics";
$INFLUXDB_ORG="metrics";
$INFLUXDB_DATABASE="metrics";
$INFLUXDB_GLOBAL_TAGS="host=${HOST},imsi=${HOST}";
$INFLUXDB_SSL="True";

