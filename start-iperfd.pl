#!/usr/bin/perl -w
#
# Copyright (c) 2000-2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use File::Tail;
use POSIX;

BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

#
# Run this on the iperf server node.
#
sub usage()
{
    print "Usage: start-iperfd.pl [-R]\n";
    exit(1);
}
my $optlist   = "dR";
my $debug     = 0;
my $TMPDIR    = "/var/tmp";
my $LOGFILE   = "$TMPDIR/start-iperfd.log";
my $PIDFILE   = "$TMPDIR/start-iperfd.pid";
my $IPERFIP   = "2001:1000:18:100::10";

# Daemon stuff
use libtestbed;

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"d"})) {
    $debug = 1;
}

if (! -e "/etc/.iperf-installed") {
    system("/local/repository/install-iperf.sh");
    if ($?) {
	die("Could not install iperf");
    }
}

if (defined($options{"R"})) {
    if (-e $PIDFILE) {
	my $pid = `/bin/cat $PIDFILE`;
	chomp($pid);
	system("sudo kill $pid");
	sleep(1);
    }
}
if (system("echo '$PID' > $PIDFILE")) {
    die("Could not create $PIDFILE\n");
}
my $port = 5000;
my $pidfile = "$TMPDIR/iperf.pid";
my $logfile = "$TMPDIR/iperf.log";

#
# Setup a signal handler to kill the daemon process before exiting.
#
sub handler()
{
    print "Handler\n";
    if (-e $pidfile) {
	my $pid = `/bin/cat $pidfile`;
	chomp($pid);
	system("sudo kill $pid");
	sleep(1);
    }
    unlink($PIDFILE);
    exit(0);
}
$SIG{INT}  = \&handler;
$SIG{TERM} = \&handler;
$SIG{HUP}  = \&handler;

print "Starting iperf server port $port\n";
my $command =
    "daemon -N -r -n iperf-server --delay=15 ".
    "   --output=$logfile ".
    "   --pidfile=$pidfile -- ".
    " /usr/local/bin/iperf3 -s -p $port -B $IPERFIP --forceflush ".
    "   --rcv-timeout 30000 ";
print "$command\n";
system($command);
if ($?) {
    die("Could not start iperf server on port $port");
}

#
# Watch for iperf getting stuck on a dropped connection and spitting
# out 0 Bytes 0 bits/sec forever. This tends to happen when the RAN
# goes down and the socket(s) are not cleanly reaped. iperf should
# handle this, but sadly does not.
#
if (! $debug) {
    if (TBBackGround($LOGFILE)) {
	exit(0);
    }
}

# Badness counter.
my $counter = 0;

while (1) {
    while (! -e $logfile) {
	print "Waiting for log file to appear\n";
	sleep(1);
    }
    while (! -e $pidfile) {
	print "Waiting for pid file to appear\n";
	sleep(1);
    }
    my $pid = `/bin/cat $pidfile`;
    chomp($pid);

    my $file = File::Tail->new("name" => $logfile,
			       "maxinterval" => 1,
			       "interval"    => 1,
			       "adjustafter" => 5,
			       "reset_tail"  => 0);

    while (my $line = $file->read()) {
	if ($line =~ /\s([\d\.]+) (\w+)\s+([\d\.]+) (\w+)\/sec/) {
	    print $line if ($debug);
	    my $bw     = $1;
	    my $rate   = $3;

	    if ($bw == 0 && $rate == 0) {
		$counter++;

		if ($counter > 10) {
		    print "Telling daemon to restart iperf3 child\n";
		    system("sudo kill -USR1 $pid");
		    last;
		}
	    }
	}
    }
    sleep(2);
    $counter = 0;
}
